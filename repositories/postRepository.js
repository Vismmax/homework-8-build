const { BaseRepository } = require('./baseRepository');

class PostRepository extends BaseRepository {
  constructor() {
    super('posts');
  }
}

exports.PostRepository = new PostRepository();
